package com.konradkevin.movies4you.util

class Constants {
    companion object {
        const val FIREBASE_COLLECTION_USERS = "users"
        const val FIREBASE_COLLECTION_RATINGS = "ratings"
        const val FIREBASE_COLLECTION_REVIEWS = "reviews"
        const val FIREBASE_COLLECTION_SUGGESTIONS = "suggestions"
        const val FIREBASE_FUNCTIONS_RATE_MOVIE = "rateMovie"

        const val FIREBASE_FIELD_TIMESTAMP = "timestamp"
        const val FIREBASE_FIELD_TITLE = "title"
    }
}