package com.konradkevin.movies4you.ui.authentication.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.AuthResult
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Credentials
import com.konradkevin.movies4you.data.repositories.AuthenticationRepository
import javax.inject.Inject

class SignUpViewModel @Inject constructor(private val authenticationRepository: AuthenticationRepository) : ViewModel() {
    private val emailError = MutableLiveData<Int?>()
    private val passwordError = MutableLiveData<Int?>()
    private val password2Error = MutableLiveData<Int?>()
    private val credentials = MutableLiveData<Credentials>()

    fun getEmailError(): LiveData<Int?> = emailError
    fun getPasswordError(): LiveData<Int?> = passwordError
    fun getPassword2Error(): LiveData<Int?> = password2Error
    fun getSignUp(): LiveData<Resource<AuthResult>> = Transformations.switchMap(credentials) {
        credentials.value?.let { credentials -> authenticationRepository.signUp(credentials.email, credentials.password) }
    }

    fun signUp(email: String, password: String, password2: String) {
        val emailValid = isEmailValid(email)
        val passwordValid = isPasswordValid(password)
        val password2Valid = isPassword2Valid(password, password2)

        if (emailValid && passwordValid && password2Valid) {
            credentials.postValue(Credentials(email, password))
        }
    }

    private fun isEmailValid(email: String): Boolean {
        val isValid = !email.isEmpty() && email.contains('@') && email.contains('.') && email.length >= 5

        if (!isValid) {
            emailError.postValue(R.string.authentication_sign_up_invalid_email)
        }

        return isValid
    }

    private fun isPasswordValid(password: String): Boolean {
        val isValid = !password.isEmpty() && password.length >= 2

        if (!isValid) {
            passwordError.postValue(R.string.authentication_sign_up_invalid_password)
        }

        return isValid
    }

    private fun isPassword2Valid(password: String, password2: String): Boolean {
        val isValid = !password2.isEmpty() && password == password2

        if (!isValid) {
            password2Error.postValue(R.string.authentication_sign_up_invalid_password2)
        }

        return isValid
    }
}