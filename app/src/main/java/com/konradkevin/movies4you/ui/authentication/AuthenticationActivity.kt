package com.konradkevin.movies4you.ui.authentication

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.authentication.signin.SignInFragment
import com.konradkevin.movies4you.ui.base.BaseActivity
import com.konradkevin.movies4you.ui.movies.MoviesActivity

class AuthenticationActivity : BaseActivity(R.layout.activity_authentication) {
    private val viewModel: AuthenticationViewModel by lazy {
        ViewModelProviders.of(this, injector.authenticationViewModelFactory()).get(AuthenticationViewModel::class.java)
    }

    override fun initUi() {}

    override fun initObservers() {
        viewModel.getAuth().observe(this, Observer { user ->
            user?.let { goToGameView() } ?: goToSignInView()
        })
    }

    private fun goToSignInView() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.authentication_container, SignInFragment.newInstance())
            .commitNow()
    }

    private fun goToGameView() {
        intent = Intent(this, MoviesActivity::class.java)
        startActivity(intent)
        finish()
    }
}