package com.konradkevin.movies4you.ui.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import com.konradkevin.movies4you.data.repositories.AuthenticationRepository
import javax.inject.Inject

class MoviesViewModel @Inject constructor(private val authenticationRepository: AuthenticationRepository) : ViewModel() {

    fun getAuth(): LiveData<FirebaseUser?> {
        return authenticationRepository.getAuth()
    }

    fun signOut() {
        authenticationRepository.signOut()
    }
}