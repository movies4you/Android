package com.konradkevin.movies4you.ui.movies.suggestions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.models.Movie
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_movies_suggestions_item.*

class MoviesSuggestionsAdapter: ListAdapter<Movie, MoviesSuggestionsAdapter.ViewHolder>(MoviesSuggestionsAdapter.DiffCallback()) {

    var onRateButtonClick: ((Movie, Float) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_movies_suggestions_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = getItem(position)
        holder.itemView.tag = movie
        holder.bind(movie)
    }

    inner class ViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(movie: Movie) {
            movies_suggestions_item_title.text = movie.title
            movies_suggestions_item_genres.text = movie.genres

            movies_suggestions_item_rate_1.setOnClickListener { onRateButtonClick?.invoke(movie, 1.0f) }
            movies_suggestions_item_rate_2.setOnClickListener { onRateButtonClick?.invoke(movie, 2.0f) }
            movies_suggestions_item_rate_3.setOnClickListener { onRateButtonClick?.invoke(movie, 3.0f) }
            movies_suggestions_item_rate_4.setOnClickListener { onRateButtonClick?.invoke(movie, 4.0f) }
            movies_suggestions_item_rate_5.setOnClickListener { onRateButtonClick?.invoke(movie, 5.0f) }
        }
    }

    class DiffCallback: DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Movie, newItem: Movie) = oldItem == newItem
    }
}