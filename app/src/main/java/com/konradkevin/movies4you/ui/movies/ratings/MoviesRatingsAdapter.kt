package com.konradkevin.movies4you.ui.movies.ratings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.models.Movie
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_movies_ratings_item.*

class MoviesRatingsAdapter: ListAdapter<Movie, MoviesRatingsAdapter.ViewHolder>(MoviesRatingsAdapter.DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.fragment_movies_ratings_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = getItem(position)
        holder.itemView.tag = movie
        holder.bind(movie)
    }

    inner class ViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(movie: Movie) {
            movies_ratings_item_title.text = containerView.context.getString(R.string.movies_ratings_item_title, movie.title, movie.rating.toString())
            movies_ratings_item_genres.text = movie.genres
        }
    }

    class DiffCallback: DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Movie, newItem: Movie) = oldItem == newItem
    }
}