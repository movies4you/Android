package com.konradkevin.movies4you.ui.authentication.signin

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.authentication.signup.SignUpFragment
import com.konradkevin.movies4you.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : BaseFragment(R.layout.fragment_sign_in) {
    private val viewModel: SignInViewModel by lazy {
        ViewModelProviders.of(this, injector.signInViewModelFactory()).get(SignInViewModel::class.java)
    }

    override fun initUi() {
        authentication_sign_in_sign_up_btn.setOnClickListener { goToSignUpView() }

        authentication_sign_in_btn.setOnClickListener {
            val email = authentication_sign_in_email_edt.text.toString()
            val password = authentication_sign_in_password_edt.text.toString()
            viewModel.signIn(email, password)
        }
    }

    override fun initObservers() {
        viewModel.getEmailError().observe(this, Observer { error -> showEmailError(error) })
        viewModel.getPasswordError().observe(this, Observer { error -> showPasswordError(error) })

        viewModel.getSignIn().observe(this, Observer { resource ->
            showSignInError(resource.error?.localizedMessage)
            showLoading(resource.status == Resource.Status.LOADING)
        })
    }

    private fun showEmailError(error: Int?) {
        error?.let { authentication_sign_in_email_edt.error = getString(it) }
    }

    private fun showPasswordError(error: Int?) {
        error?.let { authentication_sign_in_password_edt.error = getString(it) }
    }

    private fun showSignInError(error: String?) {
        error?.let { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() }
    }

    private fun showLoading(isLoading: Boolean) {
        authentication_sign_in_progress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun goToSignUpView() {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.authentication_container, SignUpFragment.newInstance())
            ?.commitNow()
    }

    companion object {
        @JvmStatic
        fun newInstance() = SignInFragment()
    }
}