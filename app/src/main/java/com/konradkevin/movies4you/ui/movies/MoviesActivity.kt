package com.konradkevin.movies4you.ui.movies

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.authentication.AuthenticationActivity
import com.konradkevin.movies4you.ui.base.BaseActivity
import com.konradkevin.movies4you.ui.movies.ratings.MoviesRatingsFragment
import com.konradkevin.movies4you.ui.movies.reviews.MoviesReviewsFragment
import com.konradkevin.movies4you.ui.movies.suggestions.MoviesSuggestionsFragment
import kotlinx.android.synthetic.main.activity_movies.*

class MoviesActivity : BaseActivity(R.layout.activity_movies) {

    private val viewPagerAdapter = MoviesPagerAdapter(supportFragmentManager)
    private val viewModel: MoviesViewModel by lazy {
        ViewModelProviders.of(this, injector.moviesViewModelFactory()).get(MoviesViewModel::class.java)
    }

    override fun initUi() {
        setSupportActionBar(movies_toolbar)

        viewPagerAdapter.addFragment(MoviesReviewsFragment.newInstance(), getString(R.string.movies_reviews_title))
        viewPagerAdapter.addFragment(MoviesSuggestionsFragment.newInstance(), getString(R.string.movies_suggestions_title))
        viewPagerAdapter.addFragment(MoviesRatingsFragment.newInstance(), getString(R.string.movies_ratings_title))

        movies_viewpager.adapter = viewPagerAdapter
        movies_tab_layout.setupWithViewPager(movies_viewpager)
        movies_viewpager.currentItem = 0
    }

    override fun initObservers() {
        viewModel.getAuth().observe(this, Observer { user ->
            if (user == null) goToAuthenticationView()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.movies_options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.movies_action_log_out -> {
                viewModel.signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun goToAuthenticationView() {
        intent = Intent(this, AuthenticationActivity::class.java)
        startActivity(intent)
        finish()
    }
}
