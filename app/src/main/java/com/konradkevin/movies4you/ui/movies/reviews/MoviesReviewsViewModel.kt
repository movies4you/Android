package com.konradkevin.movies4you.ui.movies.reviews

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.firebase.functions.HttpsCallableResult
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Movie
import com.konradkevin.movies4you.data.repositories.AuthenticationRepository
import com.konradkevin.movies4you.data.repositories.MoviesRepository
import javax.inject.Inject

class MoviesReviewsViewModel @Inject constructor(private val authenticationRepository: AuthenticationRepository, private val moviesRepository: MoviesRepository) : ViewModel() {

    fun getMoviesReviews(): LiveData<Resource<List<Movie>>> {
        return Transformations.switchMap(authenticationRepository.getAuth()) { firebaseUser ->
            firebaseUser?.let { moviesRepository.getMoviesReviews(it.uid) }
        }
    }

    fun rateMovie(movie: Movie, rating: Float): LiveData<Resource<HttpsCallableResult>> {
        return moviesRepository.rateMovie(movie, rating)
    }
}