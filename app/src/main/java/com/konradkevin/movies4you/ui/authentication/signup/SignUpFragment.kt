package com.konradkevin.movies4you.ui.authentication.signup

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.ui.authentication.signin.SignInFragment
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : BaseFragment(R.layout.fragment_sign_up) {
    private val viewModel: SignUpViewModel by lazy {
        ViewModelProviders.of(this, injector.signUpViewModelFactory()).get(SignUpViewModel::class.java)
    }

    override fun initUi() {
        authentication_sign_up_sign_in_btn.setOnClickListener { goToSignInView() }

        authentication_sign_up_btn.setOnClickListener {
            val email = authentication_sign_up_email_edt.text.toString()
            val password = authentication_sign_up_password_edt.text.toString()
            val password2 = authentication_sign_up_password2_edt.text.toString()
            viewModel.signUp(email, password, password2)
        }
    }

    override fun initObservers() {
        viewModel.getEmailError().observe(this, Observer { error -> showEmailError(error) })
        viewModel.getPasswordError().observe(this, Observer { error -> showPasswordError(error) })
        viewModel.getPassword2Error().observe(this, Observer { error -> showPassword2Error(error) })

        viewModel.getSignUp().observe(this, Observer { resource ->
            showSignUpError(resource.error?.localizedMessage)
            showLoading(resource.status == Resource.Status.LOADING)
        })
    }

    private fun showEmailError(error: Int?) {
        error?.let { authentication_sign_up_email_edt.error = getString(it) }
    }

    private fun showPasswordError(error: Int?) {
        error?.let { authentication_sign_up_password_edt.error = getString(it) }
    }

    private fun showPassword2Error(error: Int?) {
        error?.let { authentication_sign_up_password2_edt.error = getString(it) }
    }

    private fun showSignUpError(error: String?) {
        error?.let { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() }
    }

    private fun showLoading(isLoading: Boolean) {
        authentication_sign_up_progress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun goToSignInView() {
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.authentication_container, SignInFragment.newInstance())
            ?.commitNow()
    }

    companion object {
        @JvmStatic
        fun newInstance() = SignUpFragment()
    }
}