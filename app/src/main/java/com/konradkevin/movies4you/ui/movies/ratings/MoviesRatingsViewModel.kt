package com.konradkevin.movies4you.ui.movies.ratings

import androidx.lifecycle.*
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Movie
import com.konradkevin.movies4you.data.repositories.AuthenticationRepository
import com.konradkevin.movies4you.data.repositories.MoviesRepository
import javax.inject.Inject

class MoviesRatingsViewModel @Inject constructor(private val authenticationRepository: AuthenticationRepository, private val moviesRepository: MoviesRepository) : ViewModel() {

    fun getMoviesRatings(): LiveData<Resource<List<Movie>>> {
        return Transformations.switchMap(authenticationRepository.getAuth()) { firebaseUser ->
            firebaseUser?.let { moviesRepository.getMoviesRatings(it.uid) }
        }
    }
}