package com.konradkevin.movies4you.ui.authentication.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.AuthResult
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Credentials
import com.konradkevin.movies4you.data.repositories.AuthenticationRepository
import javax.inject.Inject

class SignInViewModel @Inject constructor(private val authenticationRepository: AuthenticationRepository) : ViewModel() {
    private val emailError = MutableLiveData<Int?>()
    private val passwordError = MutableLiveData<Int?>()
    private val credentials = MutableLiveData<Credentials>()

    fun getEmailError(): LiveData<Int?> = emailError
    fun getPasswordError(): LiveData<Int?> = passwordError
    fun getSignIn(): LiveData<Resource<AuthResult>> = Transformations.switchMap(credentials) {
        credentials.value?.let { credentials -> authenticationRepository.signIn(credentials.email, credentials.password) }
    }

    fun signIn(email: String, password: String) {
        val emailValid = isEmailValid(email)
        val passwordValid = isPasswordValid(password)

        if (emailValid && passwordValid) {
            credentials.postValue(Credentials(email, password))
        }
    }

    private fun isEmailValid(email: String): Boolean {
        val isValid = !email.isEmpty() && email.contains('@') && email.contains('.') && email.length >= 5

        if (!isValid) {
            emailError.postValue(R.string.authentication_sign_in_invalid_email)
        }

        return isValid
    }

    private fun isPasswordValid(password: String): Boolean {
        val isValid = !password.isEmpty() && password.length >= 2

        if (!isValid) {
            passwordError.postValue(R.string.authentication_sign_in_invalid_password)
        }

        return isValid
    }
}