package com.konradkevin.movies4you.ui.movies.suggestions

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Movie
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_movies_suggestions.*

class MoviesSuggestionsFragment : BaseFragment(R.layout.fragment_movies_suggestions) {
    private val viewModel: MoviesSuggestionsViewModel by lazy {
        ViewModelProviders.of(this, injector.moviesSuggestionsViewModelFactory()).get(MoviesSuggestionsViewModel::class.java)
    }

    private var moviesAdapter = MoviesSuggestionsAdapter()

    override fun onDestroyView() {
        movies_suggestions_recyclerview.adapter = null
        super.onDestroyView()
    }

    override fun initUi() {
        movies_suggestions_recyclerview.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = moviesAdapter
        }

        moviesAdapter.onRateButtonClick = { movie, rating -> viewModel.rateMovie(movie, rating) }
    }

    override fun initObservers() {
        viewModel.getSuggestions().observe(this, Observer { resource ->
            resource.data?.let { showSuggestions(it) }
            resource.error?.let { showError(it.localizedMessage) }
            showLoading(resource.status == Resource.Status.LOADING)
        })
    }

    private fun showLoading(isLoading: Boolean) {
        movies_suggestions_progress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showError(error: String?) {
        error?.let { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() }
    }

    private fun showSuggestions(movies: List<Movie>) {
        moviesAdapter.submitList(movies)
    }

    companion object {
        @JvmStatic
        fun newInstance() = MoviesSuggestionsFragment()
    }
}