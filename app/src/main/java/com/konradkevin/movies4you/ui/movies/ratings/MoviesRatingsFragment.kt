package com.konradkevin.movies4you.ui.movies.ratings

import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.konradkevin.movies4you.R
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.models.Movie
import com.konradkevin.movies4you.di.components.injector
import com.konradkevin.movies4you.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_movies_ratings.*

class MoviesRatingsFragment : BaseFragment(R.layout.fragment_movies_ratings) {
    private val viewModel: MoviesRatingsViewModel by lazy {
        ViewModelProviders.of(this, injector.moviesListViewModelFactory()).get(MoviesRatingsViewModel::class.java)
    }

    private var ratingsAdapter = MoviesRatingsAdapter()

    override fun onDestroyView() {
        movies_ratings_recyclerview.adapter = null
        super.onDestroyView()
    }

    override fun initUi() {
        movies_ratings_recyclerview.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = ratingsAdapter
        }
    }

    override fun initObservers() {
        viewModel.getMoviesRatings().observe(this, Observer { resource ->
            resource.data?.let { showMovies(it) }
            resource.error?.let { showError(it.localizedMessage) }
            showLoading(resource.status == Resource.Status.LOADING)
        })
    }

    private fun showLoading(isLoading: Boolean) {
        movies_ratings_progress.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showError(error: String?) {
        error?.let { Toast.makeText(activity, it, Toast.LENGTH_LONG).show() }
    }

    private fun showMovies(movies: List<Movie>) {
        ratingsAdapter.submitList(movies)
    }

    companion object {
        @JvmStatic
        fun newInstance() = MoviesRatingsFragment()
    }
}