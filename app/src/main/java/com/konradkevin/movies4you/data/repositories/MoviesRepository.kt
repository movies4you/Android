package com.konradkevin.movies4you.data.repositories

import androidx.lifecycle.LiveData
import com.google.firebase.functions.HttpsCallableResult
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.firebase.FirebaseService
import com.konradkevin.movies4you.data.models.Movie
import javax.inject.Inject

class MoviesRepository @Inject constructor(private val firebaseService: FirebaseService) {

    fun getMoviesReviews(userId: String): LiveData<Resource<List<Movie>>> {
        return firebaseService.getMoviesReviews(userId)
    }

    fun getMoviesSuggestions(userId: String): LiveData<Resource<List<Movie>>> {
        return firebaseService.getMoviesSuggestions(userId)
    }

    fun getMoviesRatings(userId: String): LiveData<Resource<List<Movie>>> {
        return firebaseService.getMoviesRatings(userId)
    }

    fun rateMovie(movie: Movie, rating: Float): LiveData<Resource<HttpsCallableResult>> {
        return firebaseService.callRateMovie(movie, rating)
    }
}
