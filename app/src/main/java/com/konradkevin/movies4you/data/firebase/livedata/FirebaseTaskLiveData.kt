package com.konradkevin.movies4you.data.firebase.livedata

import androidx.lifecycle.LiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.konradkevin.movies4you.data.Resource

class FirebaseTaskLiveData<T>(private val task: Task<T>): LiveData<Resource<T>>() {
    private val listener: OnCompleteListener<T> = OnCompleteListener { task ->
        when {
            task.exception != null -> postValue(Resource.error(task.exception))
            task.isCanceled -> postValue(Resource.cancelled())
            task.isSuccessful -> postValue(Resource.success(task.result))
        }
    }

    override fun onActive() {
        super.onActive()
        postValue(Resource.loading())
        task.addOnCompleteListener(listener)
    }
}