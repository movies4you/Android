package com.konradkevin.movies4you.data.firebase

import androidx.lifecycle.LiveData
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.HttpsCallableResult
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.firebase.livedata.FirebaseAuthLiveData
import com.konradkevin.movies4you.data.firebase.livedata.FirebaseCollectionLiveData
import com.konradkevin.movies4you.data.firebase.livedata.FirebaseTaskLiveData
import com.konradkevin.movies4you.data.models.Movie
import com.konradkevin.movies4you.util.Constants
import dagger.Reusable
import javax.inject.Inject

@Reusable
class FirebaseService @Inject constructor(){
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val firebaseFunctions: FirebaseFunctions = FirebaseFunctions.getInstance()
    private val firebaseFirestoreSettings = FirebaseFirestoreSettings.Builder()
        .setPersistenceEnabled(true)
        .setTimestampsInSnapshotsEnabled(true)
        .build()
    private val firebaseFirestore: FirebaseFirestore = FirebaseFirestore.getInstance().apply { firestoreSettings = firebaseFirestoreSettings }

    fun getAuth(): LiveData<FirebaseUser?> {
        return FirebaseAuthLiveData()
    }

    fun signIn(email: String, password: String): LiveData<Resource<AuthResult>> {
        val task = firebaseAuth.signInWithEmailAndPassword(email, password)
        return FirebaseTaskLiveData<AuthResult>(task)
    }

    fun signUp(email: String, password: String): LiveData<Resource<AuthResult>> {
        val task = firebaseAuth.createUserWithEmailAndPassword(email, password)
        return FirebaseTaskLiveData<AuthResult>(task)
    }

    fun signOut() {
        return firebaseAuth.signOut()
    }

    fun getMoviesReviews(userId: String): LiveData<Resource<List<Movie>>> {
        val query = firebaseFirestore
            .collection(Constants.FIREBASE_COLLECTION_USERS)
            .document(userId)
            .collection(Constants.FIREBASE_COLLECTION_REVIEWS)
            .orderBy(Constants.FIREBASE_FIELD_TITLE, Query.Direction.DESCENDING)
        return FirebaseCollectionLiveData(query, Movie::class.java)
    }

    fun getMoviesSuggestions(userId: String): LiveData<Resource<List<Movie>>> {
        val query = firebaseFirestore
            .collection(Constants.FIREBASE_COLLECTION_USERS)
            .document(userId)
            .collection(Constants.FIREBASE_COLLECTION_SUGGESTIONS)
        return FirebaseCollectionLiveData(query, Movie::class.java)
    }

    fun getMoviesRatings(userId: String): LiveData<Resource<List<Movie>>> {
        val query = firebaseFirestore
            .collection(Constants.FIREBASE_COLLECTION_USERS)
            .document(userId)
            .collection(Constants.FIREBASE_COLLECTION_RATINGS)
            .orderBy(Constants.FIREBASE_FIELD_TIMESTAMP, Query.Direction.DESCENDING)
        return FirebaseCollectionLiveData(query, Movie::class.java)
    }

    fun callRateMovie(movie: Movie, rating: Float): LiveData<Resource<HttpsCallableResult>> {
        val data = hashMapOf(
            "title" to movie.title,
            "genres" to movie.genres,
            "movieId" to movie.movieId,
            "rating" to rating
        )
        val task = firebaseFunctions.getHttpsCallable(Constants.FIREBASE_FUNCTIONS_RATE_MOVIE).call(data)
        return FirebaseTaskLiveData<HttpsCallableResult>(task)
    }
}