package com.konradkevin.movies4you.data.firebase.livedata

import androidx.lifecycle.LiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class FirebaseAuthLiveData: LiveData<FirebaseUser?>() {
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val listener: FirebaseAuth.AuthStateListener = FirebaseAuth.AuthStateListener { auth -> postValue(auth.currentUser) }

    override fun onActive() {
        super.onActive()
        firebaseAuth.addAuthStateListener(listener)
    }

    override fun onInactive() {
        super.onInactive()
        firebaseAuth.removeAuthStateListener(listener)
    }
}