package com.konradkevin.movies4you.data.repositories

import androidx.lifecycle.LiveData
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.konradkevin.movies4you.data.Resource
import com.konradkevin.movies4you.data.firebase.FirebaseService
import javax.inject.Inject

class AuthenticationRepository @Inject constructor(private val firebaseService: FirebaseService) {
    fun getAuth(): LiveData<FirebaseUser?> {
        return firebaseService.getAuth()
    }

    fun signIn(email: String, password: String): LiveData<Resource<AuthResult>> {
        return firebaseService.signIn(email, password)
    }

    fun signUp(email: String, password: String): LiveData<Resource<AuthResult>> {
        return firebaseService.signUp(email, password)
    }

    fun signOut() {
        return firebaseService.signOut()
    }
}
