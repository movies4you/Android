package com.konradkevin.movies4you.data.firebase

interface FirebaseDocument {
    var id: String
}