package com.konradkevin.movies4you.data.models

import com.konradkevin.movies4you.data.firebase.FirebaseDocument
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Movie(
    override var id: String = "",
    var movieId: Int = 0,
    var title: String = "",
    var genres: String = "",
    var rating: Float? = null,
    var timestamp: Int? = null
) : FirebaseDocument