package com.konradkevin.movies4you.data.models

data class Credentials(
    var email: String = "",
    var password: String = ""
)