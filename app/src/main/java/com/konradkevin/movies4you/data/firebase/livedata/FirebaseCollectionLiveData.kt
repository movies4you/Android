package com.konradkevin.movies4you.data.firebase.livedata

import androidx.lifecycle.LiveData
import com.konradkevin.movies4you.data.firebase.FirebaseDocument
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.konradkevin.movies4you.data.Resource

class FirebaseCollectionLiveData<T: FirebaseDocument>(private val reference: Query, private val model: Class<T>): LiveData<Resource<List<T>>>() {
    private lateinit var registration: ListenerRegistration
    private val listener: EventListener<QuerySnapshot> = EventListener { snapshot, error ->
        error?.let { postValue(Resource.error(it)) }
        snapshot?.let { snapshotNonNull ->
            val entities = snapshotNonNull.documents.mapNotNull { doc -> doc.toObject(model)?.apply { id = doc.id }}
            postValue(Resource.success(entities))
        }
    }

    override fun onActive() {
        super.onActive()
        postValue(Resource.loading())
        registration = reference.addSnapshotListener(listener)
    }

    override fun onInactive() {
        super.onInactive()
        registration.remove()
    }
}