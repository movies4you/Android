package com.konradkevin.movies4you.data

class Resource<T> private constructor(var status: Status, val data: T?, var error: Exception?) {
    enum class Status {
        SUCCESS,
        ERROR,
        LOADING,
        CANCELLED
    }

    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(Status.SUCCESS, data, null)
        fun <T> loading(): Resource<T> = Resource(Status.LOADING, null, null)
        fun <T> cancelled(): Resource<T> = Resource(Status.CANCELLED, null, null)
        fun <T> error(exception: Exception?): Resource<T> = Resource(Status.ERROR, null, exception)
    }
}