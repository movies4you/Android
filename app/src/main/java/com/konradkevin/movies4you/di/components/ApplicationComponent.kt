package com.konradkevin.movies4you.di.components

import android.content.Context
import com.konradkevin.movies4you.ui.authentication.signin.SignInViewModel
import com.konradkevin.movies4you.ui.authentication.signup.SignUpViewModel
import com.konradkevin.movies4you.di.ViewModelFactory
import com.konradkevin.movies4you.ui.authentication.AuthenticationViewModel
import com.konradkevin.movies4you.ui.movies.MoviesViewModel
import com.konradkevin.movies4you.ui.movies.ratings.MoviesRatingsViewModel
import com.konradkevin.movies4you.ui.movies.reviews.MoviesReviewsViewModel
import com.konradkevin.movies4you.ui.movies.suggestions.MoviesSuggestionsViewModel
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component()
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder
        fun build(): ApplicationComponent
    }

    fun authenticationViewModelFactory(): ViewModelFactory<AuthenticationViewModel>
    fun signInViewModelFactory(): ViewModelFactory<SignInViewModel>
    fun signUpViewModelFactory(): ViewModelFactory<SignUpViewModel>
    fun moviesViewModelFactory(): ViewModelFactory<MoviesViewModel>
    fun moviesSuggestionsViewModelFactory(): ViewModelFactory<MoviesSuggestionsViewModel>
    fun moviesReviewsViewModelFactory(): ViewModelFactory<MoviesReviewsViewModel>
    fun moviesListViewModelFactory(): ViewModelFactory<MoviesRatingsViewModel>
}