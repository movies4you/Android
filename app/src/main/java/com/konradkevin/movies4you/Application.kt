package com.konradkevin.movies4you

import android.app.Application
import com.konradkevin.movies4you.di.components.ApplicationComponent
import com.konradkevin.movies4you.di.components.DaggerApplicationComponent
import com.konradkevin.movies4you.di.components.DaggerComponentProvider

class Application: Application(), DaggerComponentProvider {
    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .build()
    }
}